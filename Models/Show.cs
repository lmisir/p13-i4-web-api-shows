namespace ShowsAPI.Models;

public class Show {
  public int Id { get; set; }
  public string? Name { get; set; }
  public int Year { get; set; }
  public string? Director { get; set; }
}